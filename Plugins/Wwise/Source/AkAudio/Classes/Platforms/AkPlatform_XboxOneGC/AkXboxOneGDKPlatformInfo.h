/*******************************************************************************
The content of the files in this repository include portions of the
AUDIOKINETIC Wwise Technology released in source code form as part of the SDK
package.

Commercial License Usage

Licensees holding valid commercial licenses to the AUDIOKINETIC Wwise Technology
may use these files in accordance with the end user license agreement provided
with the software or, alternatively, in accordance with the terms contained in a
written agreement between you and Audiokinetic Inc.

Copyright (c) 2021 Audiokinetic Inc.
*******************************************************************************/

#pragma once

#include "Platforms/AkPlatformInfo.h"
#include "AkXboxOneGDKPlatformInfo.generated.h"

UCLASS()
class UAkXboxOneGDKPlatformInfo : public UAkPlatformInfo
{
	GENERATED_BODY()

public:
	UAkXboxOneGDKPlatformInfo()
	{
		WwisePlatform = "XboxOneGDK";

#ifdef AK_XBOXONEGC_VS_VERSION
		Architecture = "XboxOneGC_" AK_XBOXONEGC_VS_VERSION;
#else
		Architecture = "XboxOneGC_vc150";
#endif

		LibraryFileNameFormat = "{0}.dll";
		DebugFileNameFormat = "{0}.pdb";

#if WITH_EDITOR
		// XboxOneGDK uses the XboxOne sound data.
		UAkPlatformInfo::UnrealNameToWwiseName.Add("XboxOneGDK", "XboxOne");
#endif
	}
};

UCLASS()
class UAkXboxOneAnvilPlatformInfo : public UAkXboxOneGDKPlatformInfo
{
	GENERATED_BODY()
		UAkXboxOneAnvilPlatformInfo()
	{
#if WITH_EDITOR
		// XboxOneAnvil uses the XboxOne sound data.
		UAkPlatformInfo::UnrealNameToWwiseName.Add("XboxOneAnvil", "XboxOne");
#endif
	}
};
