/*******************************************************************************
The content of the files in this repository include portions of the
AUDIOKINETIC Wwise Technology released in source code form as part of the SDK
package.

Commercial License Usage

Licensees holding valid commercial licenses to the AUDIOKINETIC Wwise Technology
may use these files in accordance with the end user license agreement provided
with the software or, alternatively, in accordance with the terms contained in a
written agreement between you and Audiokinetic Inc.

Copyright (c) 2021 Audiokinetic Inc.
*******************************************************************************/

#pragma once

#include "Platforms/AkPlatformInfo.h"
#include "AkWinGDKPlatformInfo.generated.h"

UCLASS()
class UAkWinGDKPlatformInfo : public UAkPlatformInfo
{
	GENERATED_BODY()

public:
	UAkWinGDKPlatformInfo()
	{
		WwisePlatform = "Windows";

#ifdef AK_WINGC_VS_VERSION
		Architecture = "WinGC_" AK_WINGC_VS_VERSION;
#else
		Architecture = "WinGC_vc150";
#endif

		LibraryFileNameFormat = "{0}.dll";
		DebugFileNameFormat = "{0}.pdb";

#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("WinGDK", "Windows");
#endif
	}
};

UCLASS()
class UAkWinAnvilPlatformInfo : public UAkWinGDKPlatformInfo
{
	GENERATED_BODY()
	UAkWinAnvilPlatformInfo()
	{
#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("WinAnvil", "Windows");
#endif
	}
};
