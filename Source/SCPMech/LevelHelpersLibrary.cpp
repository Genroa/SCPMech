// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelHelpersLibrary.h"


void ULevelHelpersLibrary::updateLevelTransform(ULevelStreaming* streamingLevel, FVector offsetTransform) {
    ULevel* level = streamingLevel->GetLoadedLevel();
    if (!level)
        return;

    level->ApplyWorldOffset(offsetTransform, false);
    return;
}
