// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SCPMech : ModuleRules
{
	public SCPMech(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
