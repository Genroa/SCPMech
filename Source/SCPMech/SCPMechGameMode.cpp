// Copyright Epic Games, Inc. All Rights Reserved.

#include "SCPMechGameMode.h"
#include "SCPMechHUD.h"
#include "SCPMechCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASCPMechGameMode::ASCPMechGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASCPMechHUD::StaticClass();
}
