// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SCPMechHUD.generated.h"

UCLASS()
class ASCPMechHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASCPMechHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

