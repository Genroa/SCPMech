// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelStreaming.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LevelHelpersLibrary.generated.h"

/**
 * 
 */
UCLASS()
class SCPMECH_API ULevelHelpersLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	public:

		UFUNCTION(BlueprintCallable, Category = "Test")
		static void updateLevelTransform(ULevelStreaming* streamingLevel, FVector offsetTransform);
};
