// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SCPMechGameMode.generated.h"

UCLASS(minimalapi)
class ASCPMechGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASCPMechGameMode();
};



